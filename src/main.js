var test = false;
var returnMessage = "";
var targetPitch = 0;
var targetRotation = 0;
var targetTranslation = 0;
var targetPanX = 0;
var targetPanY = 0;
var targetRotationOnMouseDown = 0;
var radiusEffect = 0;
var mouseX = 0;
var mouseY = -1;
var prevMouseX = -1;
var prevMouseY = -1;
var mouseXOnMouseDown = 0;
var windowHalfX = window.innerWidth / 2;
var windowHalfY = window.innerHeight / 2;
var btnCode = -1;
var SCREEN_WIDTH = window.innerWidth;
var SCREEN_HEIGHT = window.innerHeight;
var FLOOR = -1000;

var container;
var planeMesh;
var stats;

var camera;
var scene;
var webglRenderer;

var directionalLight, pointLight;

var windowHalfX = window.innerWidth >> 1;
var windowHalfY = window.innerHeight >> 1;

var render_gl = 1;
var has_gl = 0;

var r = 0;

var cubeMesh;
var textureCube;
var waterMesh;
var results="";
/*
* Top level export object
*/
MSV = (function () {

	/*
	* The primary milled object
	*/
    exports = {

		/*
		* added as simple way to confirm module is loaded
		*/
        Loaded: function () {
            return "sub-vis-js is loaded";
        },

        RunInTestMode: function () {
            test = true;
        },

		/*
		* primary function that recalculates the feature based on the updated JSON data model
		*/
        UpdateFeature: function () {

            if (test == true) {
                returnMessage = "";
                var t0 = performance.now(); //use this for performance timing profiles
            }
            // select tiling type
            switch (this.Properties.UserInputs.Type) {
                case 100:
                    results = this.Mill();
                    break;
                default:
                    results = this.Mill();
            }
            if (test == true) {
                var t1 = performance.now(); //use this for performance timing profiles
            }

            if (test == true) {
                console.log('EXECUTION TIME:' + (performance.now() - t0) + " milliseconds.");
                returnMessage += "syd-tile t1:" + (t1 - t0) + " milliseconds";
                return returnMessage;
            }

            if (results != "success") {
                console.log(results);
                return results; //perhaps this returns only in dev
            }
            return results
        },

		/*
		* Properties required to run milling simulation are currently in this Properties object.
		*/
        Properties: {
            Box: {
                Geometry: []
            },
            // This object stores the user inputs, the specification for the milling. 
            UserInputs: {
                //in millimeters
                CutGeometries: [
                    [[65.0875, 25.264932704, 0], [1.5875, 0, 0], [1.5875, 0, 0], [65.0875, 25.264932704, 0]]
                ],
                Toolpaths: [
                    [[65.0875, 25.264932704, 0], [1.5875, 0, 0], [1.5875, 0, 0], [65.0875, 25.264932704, 0]]
                ],
                Blocksize: [100, 100, 100],
                Blockdensity: 4.0
            },
        },

		/*
		 * Set the user data properties via a measurements key-value data pair object
		 */
        SetUserDataProperties: function (userdata) {
            var obj = JSON.stringify(userdata);
            var properties = JSON.parse(obj);
            for (var i = 0; i < properties.length; i++) {
                switch (properties[i].label) {
                    case "Type":
                        this.Properties.UserInputs.Type = parseInt(properties[i].value);
                        break;
                    default:
                        console.log("No such user data property: " + properties[i].label);
                }
            }
        },

		/*
		 * Set the user data properties via any JSON string that includes them
		 */
        SetUserDataPropertiesJSONString: function (jsonStr) {
            var obj = JSON.parse(jsonStr);
            //console.log("Got JSON String: " + jsonStr);
            this.Properties.UserInputs.Type = parseInt(obj.UserInputs.Type);
        },

		/*
		 * Get all the properties (in the recommended manner)
		 */
        GetAllProperties: function () {
            return this.GetAllPropertiesAsJSONString();
        },

		/*
		 * Get all the properties as encoded in a JSON string
		 */
        GetAllPropertiesAsJSONString: function () {
            var jsonStr = JSON.stringify(this.Properties);
            return jsonStr;
        },

 		Mill: function () {
            if (test == true) {
                var t0 = performance.now(); //use this for performance timing profiles
            }
            console.log("Visualizing the Feature");
            
            init();
            
            function addMesh( geometry, scale, x, y, z, rx, ry, rz, material ) {
                
                mesh = new THREE.Mesh( geometry, material );
                mesh.scale.x = mesh.scale.y = mesh.scale.z = scale;
                mesh.position.x = x;
                mesh.position.y = y;
                mesh.position.z = z;
                mesh.rotation.x = rx;
                mesh.rotation.y = ry;
                mesh.rotation.z = rz;
                mesh.overdraw = true;
                mesh.doubleSided = false;
                mesh.updateMatrix();
                scene.addObject(mesh);

                return mesh;
            }
                
            function changeAnim(label) {
                if (label == "stop") {
                    md.stop();
                    mdw.stop();
                    return;
                }
                md.gotoAndPlay(label, true);
                mdw.gotoAndPlay(label, true);
            }
                
            function init() {

                container = document.createElement('div');
                document.body.appendChild(container);
                
                var aspect = SCREEN_WIDTH / SCREEN_HEIGHT;

                camera = new THREE.Camera( 75, aspect, 1, 100000 );
                camera.position.z = 5000;
                camera.position.x = 0;
                camera.position.y = FLOOR+2750;

                scene = new THREE.Scene();

                scene.fog = new THREE.Fog( 0xffffff, 0, 8000 );

                // LIGHTS
                directionalLight = new THREE.DirectionalLight();
                directionalLight.position.set(0, 5000, 100);
                //var ambient = new THREE.AmbientLight( 0xffffff );
                scene.addLight( directionalLight );

                var cube = new Cube( 1, 1, 1, 1, 1 );
                cubeMesh = addMesh( cube, 1,  0, FLOOR, 0, 0,0,0, new THREE.MeshLambertMaterial( { color: 0x000000 } ) );
                cubeMesh.visible = false;
                camera.target = cubeMesh;

                canvasElement = document.getElementById('outline');
                context = canvasElement.getContext("2d"); // Grab the 2d canvas context
                //var imgd = context.getImageData(x, y, width, height);

                var imgd = context.getImageData(111, 11, 128, 128);
                var pix = imgd.data;

                //alert(pix);

                // terrain
                var img = new Image();
                img.onload = function () {
                    var data = getHeightData(img);
                    var pixctr = 0;

                    plane = new Plane( 50, 50, 127, 127 );
                    //alert(plane.vertices.length);
                    for ( var i = 0, l = plane.vertices.length; i < l; i++ ) {
                        plane.vertices[i].position.z = pix[pixctr]/100.0;
                        pixctr += 4;
                    }

                    planeMesh = addMesh( plane, 100,  0, FLOOR, 0, -1.57,0,0, getTerrainMaterial() );
                    animate();
                };
                img.src = "textures/heightmap_128.jpg";

                try {
                    webglRenderer = new THREE.WebGLRenderer( { scene: scene, clearColor: 0x000000, clearAlpha: 0.6 } );
                    webglRenderer.setFaceCulling(0);
                    webglRenderer.setSize( SCREEN_WIDTH, SCREEN_HEIGHT );
                    container.appendChild( webglRenderer.domElement );
                    has_gl = 1;
                }
                catch (e) {
                    // need webgl
                    document.getElementById('info').innerHTML = "<P><BR><B>Note.</B> You need a modern browser that supports WebGL for this to run the way it is intended.<BR>For example. <a href='http://www.google.com/landing/chrome/beta/' target='_blank'>Google Chrome 9+</a> or <a href='http://www.mozilla.com/firefox/beta/' target='_blank'>Firefox 4+</a>.</P><CENTER><BR><img src='../general/WebGL_logo.png' border='0'></CENTER>";
                    document.getElementById('info').style.display = "block";
                    return;
                }

                stats = new Stats();
                stats.domElement.style.position = 'absolute';
                stats.domElement.style.top = '0px';
                stats.domElement.style.zIndex = 100;
                container.appendChild( stats.domElement );
                
                container.addEventListener( 'mousedown', onDocumentMouseDown, false );
                container.addEventListener( 'mousewheel', onMouseWheel, false );
                container.addEventListener( 'DOMMouseScroll', onMouseWheel, false ); // firefox
            }

            function getHeightData(img) {
                var canvas = document.createElement( 'canvas' );
                canvas.width = 128;
                canvas.height = 128;
                var context = canvas.getContext( '2d' );

                var size = 128 * 128, data = new Float32Array( size );

                context.drawImage(img,0,0);

                for ( var i = 0; i < size; i ++ ) {
                    data[i] = 0
                }

                var imgd = context.getImageData(0, 0, 128, 128);
                var pix = imgd.data;

                var j=0;
                for (var i = 0, n = pix.length; i < n; i += (4)) {
                    var all = pix[i]+pix[i+1]+pix[i+2];
                    data[j++] = all/30;
                }

                return data;
            }

            function getWireframeMaterial () {
                return new THREE.MeshLambertMaterial( { color:0x231ad4, opacity: 0.65, shading: THREE.FlatShading } );
            }

            function getWaterMaterial () {
                var waterMaterial = new THREE.MeshPhongMaterial( { map: new THREE.Texture(null, THREE.UVMapping, THREE.RepeatWrapping, THREE.RepeatWrapping), ambient: 0x666666, specular: 0xffffff, env_map: textureCube, combine: THREE.Mix, reflectivity: 0.15 , opacity: 0.8, shininess: 10, shading: THREE.SmoothShading } );

                var img = new Image();
                waterMaterial.map.image = img;
                img.onload = function () {
                    waterMaterial.map.image.loaded = 1;
                };
                img.src = "textures/water.jpg";

                return waterMaterial;
            }

            function getTerrainMaterial () {
                var terrainMaterial = new THREE.MeshPhongMaterial( { map: new THREE.Texture(null, THREE.UVMapping, THREE.RepeatWrapping, THREE.RepeatWrapping), ambient: 0xffffff, specular: 0xffffff, shininess: 0, shading: THREE.SmoothShading } );

                var img = new Image();
                terrainMaterial.map.image = img;
                img.onload = function () {
                    terrainMaterial.map.image.loaded = 1;
                };
                img.src = "textures/grasslight-big.jpg"
                //img.src = "textures/terrain.jpg";

                return terrainMaterial;
            }

            function animate() {
                requestAnimationFrame( animate );
                update();
            }

            function update() {
                var dist = 4000;

                planeMesh.rotation.x += ( targetPitch - cubeMesh.rotation.x );
                planeMesh.rotation.y += ( targetRotation - cubeMesh.rotation.y ) * 0.05;
                planeMesh.position.x += ( targetPanX );
                planeMesh.position.y += ( targetPanY );
                planeMesh.position.z += ( targetTranslation );
                targetTranslation = 0;
                targetPitch = 0;
                targetRotation = 0;
                targetTranslation = 0;
                targetPanX = 0;
                targetPanY = 0;				
                
                if ( render_gl && has_gl ) {
                    webglRenderer.render( scene, camera );
                }
                stats.update();
            }

            return "success";
        }
    }
    return exports;
})();

function assignUVs(geometry) {

    geometry.faceVertexUvs[0] = [];
    var face_counter = 0;
    geometry.faces.forEach(function (face) {
        var components = ['x', 'y', 'z'].sort(function (a, b) {
            return Math.abs(face.normal[a]) > Math.abs(face.normal[b]);
        });

        var v1 = geometry.vertices[face.a];
        var v2 = geometry.vertices[face.b];
        var v3 = geometry.vertices[face.c];

        geometry.faceVertexUvs[0].push([
            new THREE.Vector2(v1[components[0]], v1[components[1]]),
            new THREE.Vector2(v2[components[0]], v2[components[1]]),
            new THREE.Vector2(v3[components[0]], v3[components[1]])
        ]);
    });
    geometry.uvsNeedUpdate = true;
}

function onWindowResize() {
    /*
        windowHalfX = window.innerWidth / 2;
        windowHalfY = window.innerHeight / 2;
        camera.aspect = window.innerWidth / window.innerHeight;
        camera.updateProjectionMatrix();
        webglRenderer.setSize( window.innerWidth, window.innerHeight );
    */
}
//
function onDocumentMouseDown( event ) {
    event.preventDefault();
    
    var mouse = new THREE.Vector2();

    var rect = container.getBoundingClientRect();    
    mouse.x = ( (event.clientX - rect.left) / webglRenderer.domElement.clientWidth ) * 2 - 1;
    mouse.y = - ( (event.clientY - rect.top) / webglRenderer.domElement.clientHeight ) * 2 + 1;

    var event = event || window.event;
    if ('object' === typeof event) {
        btnCode = event.button;

        switch (btnCode) {
            case 0:
                //console.log('Left button clicked.');
            break;

            case 1:
                //console.log('Middle button clicked.');
            break;

            case 2:
                //console.log('Right button clicked.');
            break;

            default:
                //console.log('Unexpected code: ' + btnCode);
        }
    }

    container.addEventListener( 'mousemove', onDocumentMouseMove, false );
    container.addEventListener( 'mouseup', onDocumentMouseUp, false );
    container.addEventListener( 'mouseout', onDocumentMouseOut, false );
    mouseXOnMouseDown = event.clientX - windowHalfX;
    targetRotationOnMouseDown = targetRotation;
}
function onDocumentMouseMove( event ) {

    switch (btnCode) {

    case 1: //Middle button
        if(prevMouseX == -1) {
            prevMouseX = event.clientX;
            mouseX = event.clientX;
        } else {
            prevMouseX = mouseX;
            mouseX = event.clientX;
            targetPanX = (mouseX - prevMouseX) * 10;
        }
        if(prevMouseY == -1) {
            prevMouseY = event.clientY;
            mouseY = event.clientY;
        } else {
            prevMouseY = mouseY;
            mouseY = event.clientY;
            targetPanY = (prevMouseY - mouseY) * 10;
        }
    break;
    case 2: //Right button
        if(prevMouseY == -1) {
            prevMouseY = event.clientY;
            mouseY = event.clientY;
        } else {
            prevMouseY = mouseY;
            mouseY = event.clientY;
            targetTranslation += (prevMouseY - mouseY) * 10.0;
        }
    break;
    case 0:  //Left button
    default: //or some other unexpected button event
        mouseX = event.clientX - windowHalfX;
        
        if(prevMouseY == -1) {
            prevMouseY = event.clientY;
            mouseY = event.clientY;
        } else {
            prevMouseY = mouseY;
            mouseY = event.clientY;
            targetPitch += (mouseY - prevMouseY) * 0.002;
        }
        targetRotation = targetRotationOnMouseDown + ( mouseX - mouseXOnMouseDown ) * 0.002;
    }
}
function onDocumentMouseUp( event ) {
    container.removeEventListener( 'mousemove', onDocumentMouseMove, false );
    container.removeEventListener( 'mouseup', onDocumentMouseUp, false );
    container.removeEventListener( 'mouseout', onDocumentMouseOut, false );
    prevMouseY = -1; prevMouseX = -1; mouseX = -1; mouseY = -1; targetPanX = 0; targetPanY = 0;
}
function onDocumentMouseOut( event ) {
    container.removeEventListener( 'mousemove', onDocumentMouseMove, false );
    container.removeEventListener( 'mouseup', onDocumentMouseUp, false );
    container.removeEventListener( 'mouseout', onDocumentMouseOut, false );
}
function onMouseWheel( event ) {
    var delta = Math.max(-1, Math.min(1, (event.wheelDelta || -event.detail)));
    targetTranslation += delta * 5.0;
}
function resetView() {
    targetPitch = 0;
    targetRotation = 0;
    targetTranslation = 0;
    targetPanX = 0;
    targetPanY = 0;
    targetRotationOnMouseDown = 0;
    mouseX = 0;
    mouseY = -1;
    //prevMouseX = -1;
    //prevMouseY = -1;
    mouseXOnMouseDown = 0;
}
function zoomIn() {
    targetTranslation += 10.0;
}
function zoomOut() {
    targetTranslation -= 10.0;
}
function pitchUp() {
    targetPitch += .03;
}
function pitchDown() {
    targetPitch -= .03;
}
function yawRight() {
    targetRotation += .03;
}
function yawLeft() {
    targetRotation -= .03;
}
function onMouseWheel(event) {
    var delta = Math.max(-1, Math.min(1, (event.wheelDelta || -event.detail)));
    targetTranslation += delta * 5.0;
}
function onDocumentTouchStart(event) {
    if (event.touches.length == 1) {
        event.preventDefault();
        // HEAD's UP DISPLAY HERE
        // find intersections
        var mouse = new THREE.Vector2();

        var rect = document.getElementById('container').getBoundingClientRect();
        mouse.x = ((event.touches[0].pageX - rect.left) / webglRenderer.domElement.clientWidth) * 2 - 1;
        mouse.y = - ((event.touches[0].pageY - rect.top) / webglRenderer.domElement.clientHeight) * 2 + 1;

        var raycaster = new THREE.Raycaster();
        raycaster.setFromCamera(mouse, camera);

        var intersects = raycaster.intersectObjects(objects);

        //map mouse clicks to HUD interaction handlers
        if (intersects.length > 0) {
            if (mouse.y > .74) {
                resetView();
            } else if (mouse.y < -.759) {
                if (mouse.x < .76) {
                    zoomIn();
                } else {
                    zoomOut();
                }
            } else if (mouse.y > -.536) {
                pitchUp();
            } else if (mouse.y < -.665) {
                pitchDown();
            } else {
                if (mouse.x < .76) {
                    yawLeft();
                } else {
                    yawRight();
                }
            }
        }
        mouseXOnMouseDown = event.touches[0].pageX - windowHalfX;
        targetRotationOnMouseDown = targetRotation;
    }
}
function onDocumentTouchMove(event) {
    if (event.touches.length == 1) {
        event.preventDefault();
        mouseX = event.touches[0].pageX - windowHalfX;
        if (prevMouseY == -1) {
            prevMouseY = event.touches[0].pageY;
            mouseY = event.touches[0].pageY;
        } else {
            prevMouseY = mouseY;
            mouseY = event.touches[0].pageY;
            targetPitch += (mouseY - prevMouseY) * 0.002;
        }
        targetRotation = targetRotationOnMouseDown + (mouseX - mouseXOnMouseDown) * 0.002;
    }
}
function resetView() {
    planeMesh.rotation.x = 0;
    planeMesh.rotation.y = 0;
    planeMesh.position.x = 0;
    planeMesh.position.y = 0;
    planeMesh.position.z = 0;
    targetTranslation = 0; targetPanX = 0; targetPanY = 0; targetRotation = 0; targetPitch = 0;
    webglRenderer.render(scene, camera);
}

function distance_between_points(p1, p2) {
	return Math.sqrt(((p2[0]-p1[0]) * (p2[0]-p1[0])) + ((p2[1]-p1[1]) * (p2[1]-p1[1])) + ((p2[2]-p1[2]) * (p2[2]-p1[2])));
}

function cross(a, b) {
	c = [0,0,0];
	c[0] = a[1] * b[2] - a[2] * b[1];
	c[1] = a[2] * b[0] - a[0] * b[2];
	c[2] = a[0] * b[1] - a[1] * b[0];
	return c;
}

function normalize(v) {
	length = Math.sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2]);
	v[0] = v[0]/length;
	v[1] = v[1]/length;
	v[2] = v[2]/length;
	return v;
}