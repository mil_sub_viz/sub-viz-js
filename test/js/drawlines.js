var x0=0, y0=0, x1=0, y1=0;
var t = 1;  //line depth 
var t_prev = -1;
var tn = 0; //tool number
var ct = 0; //current tool
var tp = [ //tool profile
    [.1, .2, .3, .45, .65, 1.0, .65, .45, .3, .2, .1],
    [.2, .6, .85, 1.0, .85, .6, .2],
    [.2, .6, .85, 1.0, .85, .6, .2]
];
canvasElement = document.getElementById('outline');
context = canvasElement.getContext("2d"); // Grab the 2d canvas context

function plot(x, y, c, t) {
    var pixelData = context.getImageData(x, y, 1, 1).data
    if(1.0-c*t < pixelData[0]/255) { //only update pixel if deeper than existing
        var intensity = 255 - Math.floor(c*255*t);
        context.fillStyle = "rgb(" + intensity + "," + intensity + "," + intensity + ")";
        context.fillRect(x, y, 1, 1);
    }
}

// fractional part of x
function fpart(x) {
    return x - Math.floor(x);
}

function rfpart(x) {
    return 1.0 - fpart(x);
}

function drawPass(a,b,c,d,e,f) {
    t=f;
    var steep = false;

    if (Math.abs(d - b) > Math.abs(c - a)) {
        steep = true;
    }

    var n=Math.round((tp[ct].length/2))-1; //the tool centerline
    m=0;
    //Dig out the toolcut with anti-aliasing
    for(m=1-n; m<n; m++) {
        if (steep==true) {
            drawLine(a+m,b,c+m,d,tp[ct][n+m]);
        } else {
            drawLine(a,b+m,c,d+m,tp[ct][n+m]);
        }
        t_prev = tp[ct][n+m];
    }
}

function drawLine(a,b,c,d,e) {
    x0 = a; y0 = b; x1 = c; y1 = d; t=e;
    var steep = false;

    if (Math.abs(y1 - y0) > Math.abs(x1 - x0)) {
        steep = true;
    }

    if (x0 > x1) {
        //swap the points to always draw from left to right
        var temp = x0; x0 = x1; x1 = temp;
        temp = y0; y0 = y1; y1 = temp;
    }

    if (steep==true) {
        //swap the x and y to make less steep
        var temp = x0; x0 = y0; y0 = temp;
        temp = x1; x1 = y1; y1 = temp;
    }
  
    if (x0 > x1) {
        //swap the points to always draw from left to right
        var temp = x0; x0 = x1; x1 = temp;
        temp = y0; y0 = y1; y1 = temp;
    }   

    var dx = x1 - x0;
    var dy = y1 - y0;
    var gradient = dy / dx;
    if (dx == 0.0) {
        gradient = 1.0;
    }

    // handle first endpoint
    var xend = Math.round(x0);
    var yend = y0 + gradient * (xend - x0);
    var xgap = rfpart(x0 + 0.5);
    var xpxl1 = xend; // this will be used in the main loop
    var ypxl1 = Math.floor(yend);
    /*
      If this is the center line, use the anti-aliasing
      Otherwise, use a blend with a previous neighboring line drawn
    */
    if (steep==true) {
        if(t_prev < 0) {
            plot(ypxl1, xpxl1, fpart(yend) * xgap, t);
        } else {
            plot(ypxl1, xpxl1, ((t + t_prev)/2.0), 1);
        }
        plot(ypxl1+1, xpxl1, fpart(yend) * xgap, t);        
    } else {
        if(t_prev < 0) {
            plot(xpxl1, ypxl1, rfpart(yend) * xgap, t);
        } else {
            plot(xpxl1, ypxl1, ((t + t_prev)/2.0), 1);
        }
        plot(xpxl1, ypxl1+1, fpart(yend) * xgap, t);
    }
    var intery = yend + gradient; // first y-intersection for the main loop
    
    // handle second endpoint
    xend = Math.round(x1);
    yend = y1 + gradient * (xend - x1);
    xgap = fpart(x1 + 0.5);
    xpxl2 = xend; //this will be used in the main loop
    ypxl2 = Math.floor(yend);
    if (steep==true) {
        if(t_prev < 0) {
            plot(ypxl2, xpxl2, rfpart(yend) * xgap, t);
        } else {
            plot(ypxl2, xpxl2, ((t + t_prev)/2.0), 1);
        }
        plot(ypxl2+1, xpxl2,  fpart(yend) * xgap, t);
    } else {
        if(t_prev < 0) {
            plot(xpxl2, ypxl2, rfpart(yend) * xgap, t);
        } else {
            plot(xpxl2, ypxl2, ((t + t_prev)/2.0), 1);
        }
        plot(xpxl2, ypxl2+1, fpart(yend) * xgap, t);
    }
    
    // main loop
    if (steep==true) {
        for (x = xpxl1+1; x<xpxl2-1; x++) {
            if(t_prev < 0) {
                plot(Math.floor(intery), x, rfpart(intery), t);
            } else {
                plot(Math.floor(intery), x, ((t + t_prev)/2.0), 1);
            }
            plot(Math.floor(intery)+1, x, fpart(intery), t);
            intery = intery + gradient;
        }
    } else {
        for (x = xpxl1+1; x<xpxl2-1; x++) {
            if(t_prev < 0) {
                plot(x, Math.floor(intery), rfpart(intery), t);
            } else {
                plot(x, Math.floor(intery), ((t + t_prev)/2.0), 1);           
            }
            plot(x, Math.floor(intery)+1, fpart(intery), t);
            intery = intery + gradient;
        }
    }
}